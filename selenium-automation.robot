*** Settings ***
Library   String
library   SeleniumLibrary

*** Variables ***
${Browser}   Chrome
${Homepage}  automationpractice.com/index.php
${Scheme}    http
${Testurl}   ${Scheme}://${Homepage}

*** Keywords ***

Open Homepage
    Open Browser    ${Testurl}    ${Browser}

*** Test Cases ***
C001 hacer click en contenedores
    Open Homepage
    Set Global Variable     @{NombresDeContenedores}        //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    :FOR     ${NombreDeContenedor}   IN   @{NombresDeContenedores}  
    \    Click Element    xpath=${NombreDeContenedor} 
    \    Wait Until Element Is Visible    xpath=//*[@id="bigpic"]
    \    Click Element     xpath=//*[@id="header_logo"]/a/img

    Close Browser


C002 Caso De Prueba nuevo
    Open Homepage
    